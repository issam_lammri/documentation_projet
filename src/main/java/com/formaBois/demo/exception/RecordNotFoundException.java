package com.formaBois.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * RecordNotFoundException is a class used to throw an exception when no data found in database for a entity ( client for example )
 *
 * @author DIB, LAMMRI, KEBABI, OUSMANE
 * @version 1.0
 * @since 2021-06-01
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class RecordNotFoundException extends Exception {

    private static final long serialVersionUID = 1L;

    /**
     * Class constructor for the class {@link RecordNotFoundException} with one parameter
     *
     * @param message message is a text that we specified when we call throw an new exception of this type
     */
    public RecordNotFoundException(String message) {
        super(message);
    }

    /**
     * Class constructor for the class {@link RecordNotFoundException} with two parameter
     *
     * @param message message is a text that we specified when we call throw an new exception of this type
     * @param t       type of the exception
     */
    public RecordNotFoundException(String message, Throwable t) {
        super(message, t);
    }
}
