package com.formaBois.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * FormaBoisApplication main application that we run to execute our application
 *
 * @author DIB, LAMMRI, KEBABI, OUSMANE
 * @version 1.0
 * @since 2021-06-01
 */
@SpringBootApplication
public class FormaBoisApplication {

	/**
	 * The application's entry point
	 *
	 * @param args args an array of command-line arguments for the application
	 */
	public static void main(String[] args) {
		SpringApplication.run(FormaBoisApplication.class, args);
	}

}
