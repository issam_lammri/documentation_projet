package com.formaBois.demo.repository;

import com.formaBois.demo.model.Facture;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * FactureRepository is an interface used to to significantly reduce the amount of boilerplate code required to implement data access layers for various persistence stores.
 *
 * @author DIB, LAMMRI, KEBABI, OUSMANE
 * @version 1.0
 * @since 2021-06-01
 */
@Repository
public interface FactureRepository
        extends CrudRepository<Facture, Long> {

    /**
     * SQL query to be executed in backend using nativeQuery and hibernate to get the total of all factures unpaid
     *
     * @return total of the factures unpaid to notif all users of our application
     */
    @Query(value = "select count(facture.id) as count from facture facture WHERE payed = 0 AND date <= DATE(NOW() - INTERVAL 2 MONTH) ", nativeQuery = true)
    int getCountAllFacturesUnpaid();

    /**
     * SQL query to be executed in backend using nativeQuery and hibernate to get all the factures unpaid
     *
     * @return List of factures unpaid to be displayed in a single page
     */
    @Query(value = "select * from facture WHERE payed = 0 AND date <= DATE(NOW() - INTERVAL 2 MONTH) ", nativeQuery = true)
    List<Facture> getAllFacturesUnpaid();

}
