package com.formaBois.demo.repository;

import com.formaBois.demo.model.Client;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * ClientRepository is an interface used to to significantly reduce the amount of boilerplate code required to implement data access layers for various persistence stores.
 *
 * @author DIB, LAMMRI, KEBABI, OUSMANE
 * @version 1.0
 * @since 2021-06-01
 */
@Repository
public interface ClientRepository
			extends CrudRepository<Client, Long> {

}
