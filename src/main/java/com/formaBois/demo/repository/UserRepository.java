package com.formaBois.demo.repository;

import com.formaBois.demo.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * UserRepository is an interface used to to significantly reduce the amount of boilerplate code required to implement data access layers for various persistence stores.
 *
 * @author DIB, LAMMRI, KEBABI, OUSMANE
 * @version 1.0
 * @since 2021-06-01
 */
@Repository
public interface UserRepository
        extends CrudRepository<User, Long> {
    Optional<User> findByUserName(String userName);
}
