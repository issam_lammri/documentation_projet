package com.formaBois.demo;

import com.formaBois.demo.model.MyUserDetails;
import com.formaBois.demo.model.User;
import com.formaBois.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * MyUserDetailsService is an class used to used to write business logic in a different layer, separated from @RestController class file. The logic for creating a service component class file is shown here
 * this class is used to verify the authentication if its valid or not
 *
 * @author DIB, LAMMRI, KEBABI, OUSMANE
 * @version 1.0
 * @since 2021-06-01
 */
@Service
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    /**
     * this function is used to verify if a user exist in database using his Username
     *
     * @param userName userName to be verified when user try to logIn
     * @return UserDetails of our user
     * @throws UsernameNotFoundException Creates a RecordNotFoundException with an associated error message.
     */
    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByUserName(userName);

        user.orElseThrow(() -> new UsernameNotFoundException("Not found: " + userName));

        return user.map(MyUserDetails::new).get();
    }
}
