package com.formaBois.demo.controller;

import com.formaBois.demo.exception.RecordNotFoundException;
import com.formaBois.demo.model.User;
import com.formaBois.demo.service.FactureService;
import com.formaBois.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;

/**
 * UserMvcController is a class of type Controller used to call the service to get the data ( informations about client depends on what method ) and call at the final the view to show all this data
 *
 * @author DIB, LAMMRI, KEBABI, OUSMANE
 * @version 1.0
 * @since 2021-06-01
 */
@Controller
@RequestMapping("/user")
public class UserMvcController {
    @Autowired
    UserService service;
    @Autowired
    FactureService serviceF;

    /**
     * this function is used to create a new user in database using method POST
     *
     * @param user is an object that will be constructed from the view (form fields) filled by the user
     * @return name of the route which be shown
     */
    @RequestMapping(path = "/createUser", method = RequestMethod.POST)
    public String createOrUpdateUser(User user) {
        service.createOrUpdateUser(user);
        return "redirect:/user";
    }

    /**
     * this function is used to delete a client in database by his id
     *
     * @param model is an object that will be used to be the intermediary between the backend ( controller )  and the frontend ( view ) to pass the data
     * @param id    id of the user that will be deleted
     * @return name of the route which be shown
     * @throws RecordNotFoundException Creates a RecordNotFoundException with an associated error message.
     */
    @RequestMapping(path = "/delete/{id}")
    public String deleteUserById(Model model, @PathVariable("id") Long id)
            throws RecordNotFoundException {
        service.deleteUserById(id);
        return "redirect:/user";
    }

    /**
     * this function is used to update information of the user in database by his id
     *
     * @param model is an object that will be used to be the intermediary between the backend ( controller )  and the frontend ( view ) to pass the data
     * @param id    id of the user that will be updated
     * @return name of the page which be shown
     * @throws RecordNotFoundException Creates a RecordNotFoundException with an associated error message.
     */
    @RequestMapping(path = {"/edit", "/edit/{id}"})
    public String editUserById(Model model, @PathVariable("id") Optional<Long> id)
            throws RecordNotFoundException {
        if (id.isPresent()) {
            User entity = service.getUserById(id.get());
            model.addAttribute("User", entity);
        } else {
            model.addAttribute("User", new User());
        }
        return "add-edit-user";
    }

    /**
     * this function is used to display all the list of clients that exist on the database
     *
     * @param session is an object that will be used to create some specific variables to be passed from the backend to the frontend
     * @param model   is an object that will be used to be the intermediary between the backend ( controller )  and the frontend ( view ) to pass the data
     * @return name of the page which be shown
     */
    @RequestMapping
    public String getAllUsers(HttpSession session, Model model) {
        List<User> list = service.getAllUsers();
        int count = serviceF.getCountAllFacturesUnpaid();
        session.setAttribute("CountFacturesUnpaid", count);
        model.addAttribute("Users", list);
        return "list-Users";
    }
}
