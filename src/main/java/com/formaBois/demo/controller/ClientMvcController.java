package com.formaBois.demo.controller;

import com.formaBois.demo.exception.RecordNotFoundException;
import com.formaBois.demo.model.Client;
import com.formaBois.demo.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Optional;


/**
 * ClientMvcController is a class of type Controller used to call the service to get the data ( informations about client depends on what method ) and call at the final the view to show all this data
 *
 * @author DIB, LAMMRI, KEBABI, OUSMANE
 * @version 1.0
 * @since 2021-06-01
 */
@Controller
@RequestMapping("/client")
public class ClientMvcController {
    @Autowired
    ClientService service;

    /**
     * this function is used to create a new client in database using method POST
     *
     * @param client is an object that will be constructed from the view (form fields) filled by the user
     * @return name of the route which be shown
     */
    @RequestMapping(path = "/createClient", method = RequestMethod.POST)
    public String createOrUpdateClient(Client client) {
        service.createOrUpdateClient(client);
        return "redirect:/client";
    }

    /**
     * this function is used to delete a client in database by his id
     *
     * @param model is an object that will be used to be the intermediary between the backend ( controller )  and the frontend ( view ) to pass the data
     * @param id id of the client that will be deleted
     * @return name of the route which be shown
     * @throws RecordNotFoundException Creates a RecordNotFoundException with an associated error message.
     */
    @RequestMapping(path = "/delete/{id}")
    public String deleteClientById(Model model, @PathVariable("id") Long id)
            throws RecordNotFoundException {
        service.deleteClientById(id);
        return "redirect:/client";
    }

    /**
     * this function is used to update information of the client in database by his id
     *
     * @param model is an object that will be used to be the intermediary between the backend ( controller )  and the frontend ( view ) to pass the data
     * @param id id of the client that will be updated
     * @return name of the page which be shown
     * @throws RecordNotFoundException Creates a RecordNotFoundException with an associated error message.
     */
    @RequestMapping(path = {"/edit", "/edit/{id}"})
    public String editClientById(Model model, @PathVariable("id") Optional<Long> id)
            throws RecordNotFoundException {
        if (id.isPresent()) {
            Client entity = service.getClientById(id.get());
            model.addAttribute("Client", entity);
        } else {
            model.addAttribute("Client", new Client());
        }
        return "add-edit-client";
    }

    /**
     * this function is used to display all the list of clients that exist on the database
     *
     * @param model is an object that will be used to be the intermediary between the backend ( controller )  and the frontend ( view ) to pass the data
     * @return name of the page which be shown
     */
    @RequestMapping
    public String getAllClients(Model model) {
        List<Client> list = service.getAllClients();

        model.addAttribute("clients", list);
        return "list-clients";
    }
}
