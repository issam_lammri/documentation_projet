package com.formaBois.demo.controller;

import com.formaBois.demo.exception.RecordNotFoundException;
import com.formaBois.demo.model.Client;
import com.formaBois.demo.model.Facture;
import com.formaBois.demo.service.ClientService;
import com.formaBois.demo.service.FactureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;

/**
 * FactureMvcController is a class of type Controller used to call the service to get the data ( informations about client depends on what method ) and call at the final the view to show all this data
 *
 * @author DIB, LAMMRI, KEBABI, OUSMANE
 * @version 1.0
 * @since 2021-06-01
 */
@Controller
@RequestMapping("/facture")
public class FactureMvcController {
    @Autowired
    FactureService service;
    @Autowired
    ClientService serviceClient;

    /**
     * this function is used to create a new facture in database using method POST
     *
     * @param facture is an object that will be constructed from the view (form fields) filled by the user
     * @return name of the route which be shown
     */
    @RequestMapping(path = "/createFacture", method = RequestMethod.POST)
    public String createOrUpdateFacture(Facture facture) {
        service.createOrUpdateFacture(facture);
        return "redirect:/facture";
    }

    /**
     * @param session is an object that will be used to create some specific variables to be passed from the backend to the frontend
     * @param model   is an object that will be used to be the intermediary between the backend ( controller )  and the frontend ( view ) to pass the data
     * @param id      id of the facture that will be deleted
     * @return name of the route which be shown
     * @throws RecordNotFoundException Creates a RecordNotFoundException with an associated error message.
     */
    @RequestMapping(path = "/delete/{id}")
    public String deleteFactureById(HttpSession session, Model model, @PathVariable("id") Long id)
            throws RecordNotFoundException {
        service.deleteFactureById(id);
        int count = service.getCountAllFacturesUnpaid();
        session.setAttribute("CountFacturesUnpaid", count);
        return "redirect:/facture";
    }

    /**
     * this function is used to update information of the facture in database by his id
     *
     * @param model is an object that will be used to be the intermediary between the backend ( controller )  and the frontend ( view ) to pass the data
     * @param id    id of the client that will be updated
     * @return name of the page which be shown
     * @throws RecordNotFoundException Creates a RecordNotFoundException with an associated error message.
     */
    @RequestMapping(path = {"/edit", "/edit/{id}"})
    public String editFactureById(Model model, @PathVariable("id") Optional<Long> id)
            throws RecordNotFoundException {
        List<Client> list = serviceClient.getAllClients();
        if (id.isPresent()) {
            Facture entity = service.getFactureById(id.get());
            model.addAttribute("Facture", entity);
            model.addAttribute("clients", list);
        } else {
            model.addAttribute("Facture", new Facture());
            model.addAttribute("clients", list);
        }
        return "add-edit-facture";
    }

    /**
     * this function is used to display all the list of factures that exist on the database
     *
     * @param session is an object that will be used to create some specific variables to be passed from the backend to the frontend
     * @param model   is an object that will be used to be the intermediary between the backend ( controller )  and the frontend ( view ) to pass the data
     * @return name of the page which be shown
     */
    @RequestMapping
    public String getAllFactures(HttpSession session, Model model) {
        List<Facture> list = service.getAllFactures();
        model.addAttribute("factures", list);
        int count = service.getCountAllFacturesUnpaid();
        session.setAttribute("CountFacturesUnpaid", count);
        return "list-factures";
    }

    /**
     * this function is used to display all the list of factures unpaid that exist on the database
     *
     * @param model is an object that will be used to be the intermediary between the backend ( controller )  and the frontend ( view ) to pass the data
     * @return name of the page which be shown
     */
    @RequestMapping(path = {"/getfacturesunpaid"})
    public String getAllFacturesUnpaid(Model model) {
        List<Facture> list = service.getAllFacturesUnpaid();
        model.addAttribute("factures", list);
        return "list-factures-unpaid";
    }

}
