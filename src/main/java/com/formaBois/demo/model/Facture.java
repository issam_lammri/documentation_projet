package com.formaBois.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

/**
 * Facture is a class used to create a facture object
 * Facture for client
 *
 * @author DIB, LAMMRI, KEBABI, OUSMANE
 * @version 1.0
 * @since 2021-06-01
 */
@Entity
@Table(name = "facture")
public class Facture implements Serializable {

    @Column(name = "adresse_facture", nullable = false)
    private String adresseFacture;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "client_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnoreProperties(value = {"applications", "hibernateLazyInitializer"})
    private Client client;
    @Column(name = "date", nullable = false)
    private Date date;
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "payed", nullable = false)
    private Integer payed;
    @Column(name = "prix", nullable = false)
    private Integer prix;


    /**
     * Class constructor for the entity class {@link Facture} with no parameters
     */
    public Facture() {

    }

    /**
     * Class constructor for the entity class {@link Facture} with parameters
     *
     * @param adresseFacture adresseFacture for the client not null for the class {@link Facture#adresseFacture}
     * @param client client object referenced for the the facture not null for the class {@link Client}
     * @param date date for the facture not null for the class {@link Facture#date}
     * @param id id in database autoIncrement not null for the class {@link Facture#id}
     * @param payed payed or unpaid status for the facture not null for the class {@link Facture#payed}
     * @param prix price for the facture not null for the class {@link Facture#prix}
     */
    public Facture(String adresseFacture, Client client, Date date, Long id, Integer payed, Integer prix) {
        this.adresseFacture = adresseFacture;
        this.client = client;
        this.date = date;
        this.id = id;
        this.payed = payed;
        this.prix = prix;
    }

    /**
     * getter for attribute {@link Facture#adresseFacture}
     *
     * @return the address Facture of the client {@link Facture#adresseFacture}
     */
    public String getAdresseFacture() {
        return adresseFacture;
    }

    /**
     * Setter to set the address of the facture in object {@link Facture}
     *
     * @param adresseFacture for the facture
     */
    public void setAdresseFacture(String adresseFacture) {
        this.adresseFacture = adresseFacture;
    }

    /**
     * getter for attribute {@link Facture#client}
     *
     * @return the client referenced to the Facture {@link Facture#client}
     */
    public Client getClient() {
        return client;
    }

    /**
     * Setter to set the client of the facture in object {@link Facture}
     *
     * @param client for the facture
     */
    public void setClient(Client client) {
        this.client = client;
    }

    /**
     * getter for attribute {@link Facture#date}
     *
     * @return date of to the Facture  {@link Facture#date}
     */
    public Date getDate() {
        return date;
    }

    /**
     * Setter to set date of facture in object {@link Facture}
     *
     * @param date for the facture
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * getter for attribute {@link Facture#id}
     *
     * @return the id of the client {@link Facture#id}
     */
    public Long getId() {
        return id;
    }

    /**
     * Setter to set id of facture in object {@link Facture}
     *
     * @param id for the facture
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * getter for attribute {@link Facture#payed}
     *
     * @return the status of the facture ( paid = 1 or unpaid = 0 ) {@link Facture#payed}
     */
    public Integer getPayed() {
        return payed;
    }

    /**
     * Setter to set status of facture in object {@link Facture}
     *
     * @param payed for the facture
     */
    public void setPayed(Integer payed) {
        this.payed = payed;
    }

    /**
     * getter for attribute {@link Facture#prix}
     *
     * @return the price of the facture {@link Facture#prix}
     */
    public Integer getPrix() {
        return prix;
    }

    /**
     * Setter to set price in object {@link Facture}
     *
     * @param prix for the facture
     */
    public void setPrix(Integer prix) {
        this.prix = prix;
    }

}
