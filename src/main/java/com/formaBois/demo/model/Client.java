package com.formaBois.demo.model;


import javax.persistence.*;

/**
 * Client is a class used to create a client object
 *
 * @author DIB, LAMMRI, KEBABI, OUSMANE
 * @version 1.0
 * @since 2021-06-01
 */
@Entity
@Table(name = "client")
public class Client {

    @Column(name = "adrress", nullable = false)
    private String adrress;
    @Column(name = "code_postal", nullable = false)
    private Integer codePostal;
    @Column(name = "email", nullable = false)
    private String email;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "nom", nullable = false)
    private String nom;
    @Column(name = "prenom", nullable = false)
    private String prenom;
    @Column(name = "reference", nullable = false)
    private String reference;
    @Column(name = "telephone", nullable = false)
    private Integer telephone;
    @Column(name = "ville", nullable = false)
    private String ville;

    /**
     * Class constructor for the entity class {@link Client} with no parameters
     */
    public Client() {

    }

    /**
     * Class constructor for the entity class {@link Client} with parameters
     *
     * @param adrress    address for the client not null for the class {@link Client#adrress}
     * @param codePostal codePostal for client's {@link Client#adrress} not null for the class {@link Client#codePostal}
     * @param email      email for the client not null for the class {@link Client#email}
     * @param id         id in database autoIncrement not null for the class {@link Client#id}
     * @param nom        lastname for the client not null for the class {@link Client#nom}
     * @param prenom     firstname for the client not null for the class {@link Client#prenom}
     * @param reference  reference for the client not null for the class {@link Client#reference}
     * @param telephone  telephone for the client not null for the class {@link Client#telephone}
     * @param ville      ville for the client's {@link Client#adrress} not null for the class {@link Client#ville}
     * @see Client#id
     */
    public Client(String adrress, Integer codePostal, String email, Long id, String nom, String prenom, String reference, Integer telephone, String ville) {
        this.adrress = adrress;
        this.codePostal = codePostal;
        this.email = email;
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.reference = reference;
        this.telephone = telephone;
        this.ville = ville;
    }

    /**
     * getter for attribute {@link Client#adrress}
     *
     * @return the address of the client {@link Client#adrress}
     */
    public String getAdrress() {
        return adrress;
    }

    /**
     * Setter to set address in object {@link Client}
     *
     * @param adrress for the client
     */
    public void setAdrress(String adrress) {
        this.adrress = adrress;
    }

    /**
     * getter for attribute {@link Client#codePostal}
     *
     * @return the codePostal of the client's Address {@link Client#codePostal}
     */
    public Integer getCodePostal() {
        return codePostal;
    }

    /**
     * Setter to set codePostal in object {@link Client}
     *
     * @param codePostal for the client
     */
    public void setCodePostal(Integer codePostal) {
        this.codePostal = codePostal;
    }

    /**
     * getter for attribute {@link Client#email}
     *
     * @return the email of the client {@link Client#email}
     */
    public String getEmail() {
        return email;
    }

    /**
     * Setter to set email in object {@link Client}
     *
     * @param email for the client
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * getter for attribute {@link Client#id}
     *
     * @return the id of the client {@link Client#id}
     */
    public Long getId() {
        return id;
    }

    /**
     * Setter to set id in object {@link Client}
     *
     * @param id for the client
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * getter for attribute {@link Client#nom}
     *
     * @return the lastname of the client {@link Client#nom}
     */
    public String getNom() {
        return nom;
    }

    /**
     * Setter to set the lastname in object {@link Client}
     *
     * @param nom for the client
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * getter for attribute {@link Client#prenom}
     *
     * @return the firstname of the client {@link Client#prenom}
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * Setter to set the firstname in object {@link Client}
     *
     * @param prenom for the client
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * getter for attribute {@link Client#reference}
     *
     * @return the reference of the client {@link Client#reference}
     */
    public String getReference() {
        return reference;
    }

    /**
     * Setter to set the reference in object {@link Client}
     *
     * @param reference for the client
     */
    public void setReference(String reference) {
        this.reference = reference;
    }

    /**
     * getter for attribute {@link Client#telephone}
     *
     * @return the phone number of the client {@link Client#telephone}
     */
    public Integer getTelephone() {
        return telephone;
    }

    /**
     * Setter to set the phone number in object {@link Client}
     *
     * @param telephone for the client
     */
    public void setTelephone(Integer telephone) {
        this.telephone = telephone;
    }

    /**
     * getter for attribute {@link Client#ville}
     *
     * @return the city of the client's address {@link Client#ville}
     */
    public String getVille() {
        return ville;
    }

    /**
     * Setter to set the city in object {@link Client}
     *
     * @param ville for the client's address
     */
    public void setVille(String ville) {
        this.ville = ville;
    }
}
