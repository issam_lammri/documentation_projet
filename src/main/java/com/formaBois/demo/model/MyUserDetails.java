package com.formaBois.demo.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * MyUserDetails is a class used to to verify (check the username,password,Active,Role ) login and logout in the application
 *
 * @author DIB, LAMMRI, KEBABI, OUSMANE
 * @version 1.0
 * @since 2021-06-01
 */
public class MyUserDetails implements UserDetails {

    private String userName;
    private String password;
    private boolean active;
    private List<GrantedAuthority> authorities;

    /**
     * Class constructor for the class {@link MyUserDetails} with parameters {@link User}
     * @param user User Object
     */
    public MyUserDetails(User user) {
        this.userName = user.getUserName();
        this.password = user.getPassword();
        this.active = user.isActive();
        this.authorities = Arrays.stream(user.getRoles().split(","))
                    .map(SimpleGrantedAuthority::new)
                    .collect(Collectors.toList());
    }

    /**
     * getter for attribute {@link MyUserDetails#authorities}
     *
     * @return the roles of the user {@link MyUserDetails#authorities}
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    /**
     * getter for attribute {@link MyUserDetails#password}
     *
     * @return the password of the user {@link MyUserDetails#password}
     */
    @Override
    public String getPassword() {
        return password;
    }

    /**
     * getter for attribute {@link MyUserDetails#userName}
     *
     * @return the username of the user {@link MyUserDetails#userName}
     */
    @Override
    public String getUsername() {
        return userName;
    }

    /**
     * getter for attribute {@link MyUserDetails#isAccountNonExpired()}
     *
     * @return state of the user ( account expired or not )  {@link MyUserDetails#isAccountNonExpired()}
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * getter for attribute {@link MyUserDetails#isAccountNonLocked()}
     *
     * @return state of the user ( account locked or not )  {@link MyUserDetails#isAccountNonLocked()}
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * getter for attribute {@link MyUserDetails#isCredentialsNonExpired()}
     *
     * @return state of the credentials user ( valid or not )  {@link MyUserDetails#isCredentialsNonExpired()}
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * getter for attribute {@link MyUserDetails#isEnabled()}
     *
     * @return state of the user ( active or not )  {@link MyUserDetails#isEnabled()}
     */
    @Override
    public boolean isEnabled() {
        return active;
    }
}
