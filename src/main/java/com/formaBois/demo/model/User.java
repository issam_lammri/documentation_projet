package com.formaBois.demo.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * User is a class used to create a User object
 *
 * @author DIB, LAMMRI, KEBABI, OUSMANE
 * @version 1.0
 * @since 2021-06-01
 */
@Entity
@Table(name = "user")
public class User implements Serializable {

    @Column(name = "active", nullable = false)
    private Boolean active;
    @Column(name = "email", nullable = false)
    private String email;
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "nom", nullable = false)
    private String nom;
    @Column(name = "password", nullable = false)
    private String password;
    @Column(name = "prenom", nullable = false)
    private String prenom;
    @Column(name = "roles", nullable = false)
    private String roles;

    @Column(name = "user_name", nullable = false)
    private String userName;


    /**
     * Class constructor for the entity class {@link User} with no parameters
     */
    public User() {

    }

    /**
     * Class constructor for the entity class {@link User} with parameters
     *
     * @param id id in database autoIncrement not null for the class {@link User#id}
     * @param nom lastname for the user not null for the class {@link User#nom}
     * @param prenom firstname for the user not null for the class {@link User#prenom}
     * @param email email for the user not null for the class {@link User#email}
     * @param password password for the user not null for the class {@link User#password}
     * @param active active is a status for the user ( active user or not )  not null for the class {@link User#active}
     * @param roles role of user ( admin or a simple user ) not null for the class {@link User#email}
     * @param userName userName for the user not null for the class {@link User#userName}
     */
    public User(Long id, String nom, String prenom, String email, String password, Boolean active, String roles, String userName) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.password = password;
        this.active = active;
        this.roles = roles;
        this.userName = userName;
    }

    /**
     * getter for attribute {@link User#email}
     *
     * @return the email of the user {@link User#email}
     */
    public String getEmail() {
        return email;
    }

    /**
     * Setter to set email of the user in object {@link User}
     *
     * @param email for the client
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * getter for attribute {@link User#id}
     *
     * @return the id of the user {@link User#id}
     */
    public Long getId() {
        return id;
    }

    /**
     * Setter to set id of the user in object {@link User}
     *
     * @param id for the client
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * getter for attribute {@link User#nom}
     *
     * @return the lastname of the user {@link User#nom}
     */
    public String getNom() {
        return nom;
    }

    /**
     * Setter to set lastname of the user in object {@link User}
     *
     * @param nom for the client
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * getter for attribute {@link User#password}
     *
     * @return the password of the user {@link User#password}
     */
    public String getPassword() {
        return password;
    }

    /**
     * Setter to set password of the user in object {@link User}
     *
     * @param password for the client
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * getter for attribute {@link User#prenom}
     *
     * @return the firstname of the user {@link User#prenom}
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * Setter to set firstname of the user in object {@link User}
     *
     * @param prenom for the client
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * getter for attribute {@link User#roles}
     *
     * @return the role of the user {@link User#roles}
     */
    public String getRoles() {
        return roles;
    }

    /**
     * Setter to set role of the user in object {@link User}
     *
     * @param roles for the client
     */
    public void setRoles(String roles) {
        this.roles = roles;
    }

    /**
     * getter for attribute {@link User#userName}
     *
     * @return the userName of the user {@link User#userName}
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Setter to set userName of the user in object {@link User}
     *
     * @param userName for the client
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * getter for attribute {@link User#active}
     *
     * @return the status of the user {@link User#active}
     */
    public Boolean isActive() {
        return active;
    }

    /**
     * Setter to set status of the user in object {@link User}
     *
     * @param active for the client
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     *
     * @return String of object user ( all the details )
     */
    @Override
    public String toString() {
        return "User{" +
                "id=" + id + '\'' +
                "nom=" + nom + '\'' +
                "prenom=" + prenom + '\'' +
                "email=" + email + '\'' +
                "password=" + password + '\'' +
                "active=" + active + '\'' +
                "roles=" + roles + '\'' +
                "userName=" + userName + '\'' +
                '}';
    }
}
