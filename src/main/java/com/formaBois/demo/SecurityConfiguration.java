package com.formaBois.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * MyUserDetailsService is an class that extends from WebSecurityConfigurerAdapter
 * this class is used to secure the logIn and logOut of our users in the application and secure the the acces of pages depends on each user ( role )
 *
 * @author DIB, LAMMRI, KEBABI, OUSMANE
 * @version 1.0
 * @since 2021-06-01
 */
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    UserDetailsService userDetailsService;

    /**
     * this function is used to get the password encoded
     *
     * @return PasswordEncoder
     */
    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }

    /**
     * this function is used to call the function of the configuration of authentication
     *
     * @param auth Allows providing a parent AuthenticationManager that will be tried if this AuthenticationManager was unable to attempt to authenticate the provided Authentication.
     * @throws Exception Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    /**
     * this function is used to verify the roles of each user and granted the acces , also granted the security of login and logout and finaly granted the redirection between pages
     *
     * @param http httpRequest
     * @throws Exception Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/admin").hasRole("ADMIN")
                .antMatchers("/user").hasAnyRole("ADMIN", "USER")
                .antMatchers("/user/createUser").hasRole("ADMIN")
                .antMatchers("/user/edit/{id}").hasRole("ADMIN")
                .antMatchers("/user/delete/{id}").hasRole("ADMIN")
                .antMatchers("/facture").hasAnyRole("ADMIN", "USER")
                .antMatchers("/user/createFacture").hasRole("ADMIN")
                .antMatchers("/user/getfacturesunpaid").hasAnyRole("ADMIN", "USER")
                .antMatchers("/facture/edit/{id}").hasRole("ADMIN")
                .antMatchers("/facture/delete/{id}").hasRole("ADMIN")
                .antMatchers("/client").hasAnyRole("ADMIN", "USER")
                .antMatchers("/user/createClient").hasRole("ADMIN")
                .antMatchers("/client/edit/{id}").hasRole("ADMIN")
                .antMatchers("/client/delete/{id}").hasRole("ADMIN")
                .antMatchers("/").permitAll()
                .and().formLogin()
                .and()
                .logout()
                .logoutSuccessUrl("/user");
    }
}