package com.formaBois.demo.service;

import com.formaBois.demo.exception.RecordNotFoundException;
import com.formaBois.demo.model.User;
import com.formaBois.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * UserService is an class used to used to write business logic in a different layer, separated from @RestController class file. The logic for creating a service component class file is shown here
 * we are using User Service API(s) to store, retrieve, update and delete the Users. We wrote the business logic in @RestController class file itself. Now, we are going to move the business logic code from controller to service component.
 *
 * @author DIB, LAMMRI, KEBABI, OUSMANE
 * @version 1.0
 * @since 2021-06-01
 */
@Service
public class UserService {

    @Autowired
    UserRepository repository;

    /**
     * this function is used to create a new User or update an existent User in database
     *
     * @param entity Object User to be used to create a new one or update the existent
     * @return User created or updated
     */
    public User createOrUpdateUser(User entity) {
        if (entity.getId() == null) {
            entity = repository.save(entity);

            return entity;
        } else {
            Optional<User> user = repository.findById(entity.getId());

            if (user.isPresent()) {
                User newEntity = user.get();
                newEntity.setEmail(entity.getEmail());
                newEntity.setPrenom(entity.getPrenom());
                newEntity.setNom(entity.getNom());
                newEntity.setUserName(entity.getUserName());
                newEntity.setActive(entity.isActive());
                newEntity.setRoles(entity.getRoles());
                newEntity.setPassword(entity.getPassword());

                newEntity = repository.save(newEntity);

                return newEntity;
            } else {
                entity = repository.save(entity);

                return entity;
            }
        }
    }

    /**
     * this function is used to delete a User from the database using his id
     *
     * @param id the id of the User to be deleted
     * @throws RecordNotFoundException Creates a RecordNotFoundException with an associated error message.
     */
    public void deleteUserById(Long id) throws RecordNotFoundException {
        Optional<User> user = repository.findById(id);

        if (user.isPresent()) {
            repository.deleteById(id);
        } else {
            throw new RecordNotFoundException("No user record exist for given id");
        }
    }

    /**
     *  this functions is used to retrieve all users that exist on the base
     * @return List of users
     */
    public List<User> getAllUsers() {
        List<User> result = (List<User>) repository.findAll();

        if (result.size() > 0) {
            return result;
        } else {
            return new ArrayList<User>();
        }
    }

    /**
     * this function is used to retrieve one user by its id
     *
     * @param id the id of the user to be retrieved
     * @return User retrieved
     * @throws RecordNotFoundException Creates a RecordNotFoundException with an associated error message.
     */
    public User getUserById(Long id) throws RecordNotFoundException {
        Optional<User> user = repository.findById(id);

        if (user.isPresent()) {
            return user.get();
        } else {
            throw new RecordNotFoundException("No user record exist for given id");
        }
    }
}