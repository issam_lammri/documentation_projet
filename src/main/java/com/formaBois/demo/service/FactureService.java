package com.formaBois.demo.service;

import com.formaBois.demo.exception.RecordNotFoundException;
import com.formaBois.demo.model.Facture;
import com.formaBois.demo.repository.FactureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * FactureService is an class used to used to write business logic in a different layer, separated from @RestController class file. The logic for creating a service component class file is shown here
 * we are using Facture Service API(s) to store, retrieve, update and delete the Factures. We wrote the business logic in @RestController class file itself. Now, we are going to move the business logic code from controller to service component.
 *
 * @author DIB, LAMMRI, KEBABI, OUSMANE
 * @version 1.0
 * @since 2021-06-01
 */
@Service
public class FactureService {

    @Autowired
    FactureRepository repository;

    /**
     * this function is used to create a new Facture or update an existent Facture in database
     *
     * @param entity Object Facture to be used to create a new one or update the existent
     * @return Facture created or updated
     */
    public Facture createOrUpdateFacture(Facture entity) {
        if (entity.getId() == null) {
            entity = repository.save(entity);

            return entity;
        } else {
            Optional<Facture> facture = repository.findById(entity.getId());

            if (facture.isPresent()) {
                Facture newEntity = facture.get();
                newEntity.setAdresseFacture(entity.getAdresseFacture());
                newEntity.setDate(entity.getDate());
                newEntity.setPayed(entity.getPayed());
                newEntity.setPrix(entity.getPrix());
                newEntity.setClient(entity.getClient());

                newEntity = repository.save(newEntity);

                return newEntity;
            } else {
                entity = repository.save(entity);

                return entity;
            }
        }
    }

    /**
     * this function is used to delete a facture from the database using his id
     *
     * @param id the id of the facture to be deleted
     * @throws RecordNotFoundException Creates a RecordNotFoundException with an associated error message.
     */
    public void deleteFactureById(Long id) throws RecordNotFoundException {
        Optional<Facture> facture = repository.findById(id);

        if (facture.isPresent()) {
            repository.deleteById(id);
        } else {
            throw new RecordNotFoundException("No facture record exist for given id");
        }
    }

    /**
     *  this functions is used to retrieve all factures that exist on the base
     * @return List of factures
     */
    public List<Facture> getAllFactures() {
        List<Facture> result = (List<Facture>) repository.findAll();

        if (result.size() > 0) {
            return result;
        } else {
            return new ArrayList<Facture>();
        }
    }

    /**
     * this function is used to retrieve one facture by its id
     *
     * @param id the id of the facture to be retrieved
     * @return Facture retrieved
     * @throws RecordNotFoundException Creates a RecordNotFoundException with an associated error message.
     */
    public Facture getFactureById(Long id) throws RecordNotFoundException {
        Optional<Facture> facture = repository.findById(id);

        if (facture.isPresent()) {
            return facture.get();
        } else {
            throw new RecordNotFoundException("No facture record exist for given id");
        }
    }

    /**
     * this function is used to retrieve the total of all factures unpaid
     *
     * @return total of all factures unpaid
     */
    public int getCountAllFacturesUnpaid(){
        int count = repository.getCountAllFacturesUnpaid();
        return count;
    }

    /**
     * this function is used to retrieve the list of all the factures unpaid
     *
     * @return List of factures
     */
    public List<Facture> getAllFacturesUnpaid(){
        List<Facture> result = repository.getAllFacturesUnpaid();
        return result;
    }
}