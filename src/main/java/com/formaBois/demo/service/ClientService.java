package com.formaBois.demo.service;

import com.formaBois.demo.exception.RecordNotFoundException;
import com.formaBois.demo.model.Client;
import com.formaBois.demo.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * ClientService is an class used to used to write business logic in a different layer, separated from @RestController class file. The logic for creating a service component class file is shown here
 * we are using Client Service API(s) to store, retrieve, update and delete the Clients. We wrote the business logic in @RestController class file itself. Now, we are going to move the business logic code from controller to service component.
 *
 * @author DIB, LAMMRI, KEBABI, OUSMANE
 * @version 1.0
 * @since 2021-06-01
 */
@Service
public class ClientService {

    @Autowired
    ClientRepository repository;

    /**
     * this function is used to create a new client or update an existent client in database
     *
     * @param entity Object Client to be used to create a new one or update the existent
     * @return Client created or updated
     */
    public Client createOrUpdateClient(Client entity) {
        if (entity.getId() == null) {
            entity = repository.save(entity);

            return entity;
        } else {
            Optional<Client> client = repository.findById(entity.getId());

            if (client.isPresent()) {
                Client newEntity = client.get();
                newEntity.setEmail(entity.getEmail());
                newEntity.setPrenom(entity.getPrenom());
                newEntity.setNom(entity.getNom());
                newEntity.setAdrress(entity.getAdrress());
                newEntity.setReference(entity.getReference());
                newEntity.setTelephone(entity.getTelephone());
                newEntity.setCodePostal(entity.getCodePostal());
                newEntity.setVille(entity.getVille());

                newEntity = repository.save(newEntity);

                return newEntity;
            } else {
                entity = repository.save(entity);

                return entity;
            }
        }
    }

    /**
     * this function is used to delete a user from the database using his id
     *
     * @param id the id of the user to be deleted
     * @throws RecordNotFoundException Creates a RecordNotFoundException with an associated error message.
     */
    public void deleteClientById(Long id) throws RecordNotFoundException {
        Optional<Client> client = repository.findById(id);

        if (client.isPresent()) {
            repository.deleteById(id);
        } else {
            throw new RecordNotFoundException("No client record exist for given id");
        }
    }

    /**
     *  this functions is used to retrieve all clients that exist on the base
     * @return List of clients
     */
    public List<Client> getAllClients() {
        List<Client> result = (List<Client>) repository.findAll();

        if (result.size() > 0) {
            return result;
        } else {
            return new ArrayList<Client>();
        }
    }

    /**
     * this function is used to retrieve one client by its id
     *
     * @param id the id of the user to be retrieved
     * @return Client retrieved
     * @throws RecordNotFoundException Creates a RecordNotFoundException with an associated error message.
     */
    public Client getClientById(Long id) throws RecordNotFoundException {
        Optional<Client> client = repository.findById(id);

        if (client.isPresent()) {
            return client.get();
        } else {
            throw new RecordNotFoundException("No client record exist for given id");
        }
    }
}